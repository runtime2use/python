#!/bin/bash

export PYTHONPATH="$LANG_PATH/dist:$LANG_INCL:$LANG_CORE"

export RONIN_VENV=$RONIN_VARI/run/venv/$RONIN_ARCH

################################################################################

ronin_require_python () {
    for pth in $RONIN_CODE/python/dist/nucleon $RONIN_TEMP/pyenv ; do
        if [[ ! -d $pth ]] ; then
            mkdir -p $pth
        fi
    done

    if [[ ! -f $RONIN_CODE/python/dist/nucleon/__init__.py ]] ; then
        touch $RONIN_CODE/python/dist/nucleon/__init__.py
    fi
}

#*******************************************************************************

ronin_include_python () {
    if [[ ! -d $RONIN_VENV ]] ; then
        virtualenv --prompt "<-RoNiN-> " --system-site-packages --no-download $RONIN_VENV
    fi

    motd_text "    -> Python  : "$PYTHONPATH

    if [[ -d $RONIN_VENV ]] ; then
        source $RONIN_VENV/bin/activate
    fi
}

################################################################################

ronin_setup_python () {
    $RONIN_VENV/bin/pip install -r $RONIN_CODE/python/requirements.txt

    #*******************************************************************************

    #for key in reactor ; do
    #    pth=$RONIN_CODE/python/dist/$key

    #    if [[ ! -d $pth ]] ; then
    #        ensure_repo 'Core' $key $pth https://bitbucket.org/NeuroFleet/$key.git
    #    fi
    #done

    #for key in `echo $APPLETs` ; do
    #    ensure_repo 'Application' $key $RONIN_ROOT/nucleon/$key https://bitbucket.org/NeuroFleet/$key.git
    #done

    #for key in `echo $PROGRAMs` ; do
    #    ensure_repo 'Program' $key $RONIN_ROOT/programs/$key https://bitbucket.org/NeuroFleet/$key.git
    #done
}

ronin_install_python () {
    for ext in `echo $GHOST_REACTOR $GHOST_FRAMES` ; do
        if [[ ! -d $GHOST_LIB/python/dist/$ext ]] ; then
            git clone https://bitbucket.org/NeuroMATE/$ext.git $GHOST_LIB/python/dist/$ext
        fi
    done

    #*******************************************************************************

    for app in $GHOST_AGENTS ; do
        if [[ ! -d $GHOST_LIB/python/dist/$GHOST_NARROW/$app ]] ; then
            git clone https://bitbucket.org/$GHOST_NARROW/$app.git $GHOST_LIB/python/dist/$GHOST_NARROW/$app
        fi
    done
}

