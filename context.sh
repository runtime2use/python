#!/bin/bash

export LANG_DEPEND="libc"

#*******************************************************************************

export EVENTLET_HUB=epolls

#export DJANGO_SETTINGS_MODULE="reactor.webapp.settings"

#*******************************************************************************

export NLTK_DATA=$RONIN_MONT/corpora/nltk

export FLASK_DEBUG=1

################################################################################

#if [[ "x$PYTHONPATH" == "x" ]] ; then
#    if [[ -d $GHOST_VENV ]] ; then
#        export PYTHONPATH="$GHOST_VENV/lib/python2.7/site-packages"
#    else
#        export PYTHONPATH="/usr/local/lib/python2.7/dist-packages"
#    fi
#fi

#*******************************************************************************

#if [[ -d $GHOST_SPECS/codes ]] ; then
#    export PYTHONPATH=$PYTHONPATH":"$GHOST_SPECS"/codes"
#fi

#for key in dist gold ; do
#    if [[ -d $GHOST_LIB/python/$key ]] ; then
#        export PYTHONPATH=$PYTHONPATH":"$GHOST_LIB"/python/"$key
#    fi
#done

