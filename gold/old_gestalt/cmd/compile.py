#!/usr/bin/python

import os, sys

import yaml, simplejson as json

if __name__=='__main__':
    package = json.loads(open('package.temp').read())
    scripts = {}

    for app in os.listdir('uchikoma'):
        if os.path.exists('uchikoma/%s/converse/manifest.yml' % app):
            cfg = yaml.load(open('uchikoma/%s/converse/manifest.yml' % app))

            for key,lst in cfg.iteritems():
                if key not in scripts:
                    scripts[key] = []

                for item in lst:
                    scripts[key].append(item)

        if os.path.exists('uchikoma/%s/converse/packages.json' % app):
            lst = json.loads(open('uchikoma/%s/converse/packages.json' % app).read())

            for name,version in lst.iteritems():
                package['dependencies'][name] = version

        if os.path.exists('uchikoma/%s/converse/scripts' % app):
            for src in os.listdir('uchikoma/%s/converse/scripts' % app):
                os.system('cp -afR uchikoma/%s/converse/scripts/%s scripts/%s' % (app,src,src))

    f = open('package.json', 'w+')
    f.write(json.dumps(package, sort_keys=True, indent=4, separators=(',', ': ')))
    f.close()

    for key,lst in scripts.iteritems():
        f = open('%s-scripts.json' % key, 'w+')
        f.write(json.dumps(lst, sort_keys=True, indent=4, separators=(',', ': ')))
        f.close()

    os.system('npm install')

