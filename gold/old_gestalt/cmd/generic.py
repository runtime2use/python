#!/usr/bin/python

import os, sys

from gestalt.shell.shortcuts import *

########################################################################################

CONFIG = {}

reactor = Nucleon.instance(
    os.environ.get('GHOST_MODULE', 'django'),
    os.environ.get('GHOST_NARROW', 'default'),
**CONFIG)

#***************************************************************************************

upath = lambda *x: os.path.join(os.environ['UCHIKOMA_ROOT'], *x)
pyenv = lambda *x: upath('var', 'pyenv', *x)

#***************************************************************************************

class Ghost(object):
    @classmethod
    def populate(cls):
        return os.listdir(os.path.join(os.environ.get('UCHIKOMA_ROOT', '/app'), 'home'))

    @classmethod
    def from_env(cls, root=None, person=None):
        if root is None:
            root = os.environ.get('UCHIKOMA_ROOT', '/app')

        if person is None:
            lst = cls.populate()

            if 'GHOST_PERSON' in os.environ:
                person = os.environ['GHOST_PERSON']
            elif len(lst):
                person = lst[0]

        return cls(root, person, os.environ.get('GHOST_MODULE', 'console'))

    def __init__(self, root, ghost, verse):
        self._root  = root
        self._ghost = ghost
        self._verse = verse

        for key,path in {
            'creds': ['credentials.yml'],
            'specs': ['manifest.yml'],
            'spine': ['implant.yml'],
        }.iteritems():
            setattr(self, key, yaml.load(open(self.rpath(*path))))

    root   = property(lambda self: self._root)
    domain = property(lambda self: os.environ.get('GHOST_DOMAIN', 'example.tld'))
    alias  = property(lambda self: self._ghost)
    module = property(lambda self: self._verse)

    bpath  = lambda self, *x: os.path.join(self._root, *x)
    rpath  = lambda self, *x: self.bpath('home', self.alias, *x)

    netloc = property(lambda self: '%s@%s' % (self.alias, self.domain))

    @property
    def context(self):
        resp = dict([
            (field, getattr(self, field))
            for field in ('root','alias','module','netloc','domain')
            if hasattr(self, field)
        ])

        return resp

    def __getitem__(self, key, default=None):
        for xyz in ('context','creds','specs','spine'):
            cnt = getattr(self, xyz)

            if key in cnt:
                return cnt[key]

        return default

    def __setitem__(self, key, value):
        cnt[key] = value

        return value

#***************************************************************************************

@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('--debug/--no-debug', default=False)
@click.option('-v', '--verbose', count=True)
@click.option('--platform', default='local', help='Platform : local , heroku.')
@click.option('--verse', default='console')
@click.pass_context
def cli(ctx, debug, *args, **kwargs):
    if os.environ['PWD']=='/app':
        ctx.obj['platform'] = 'heroku'
    else:
        ctx.obj['platform'] = 'local'

        if os.path.exists(pyenv()):
            #click.echo("VirtEnv : %s" % pyenv())

            ctx.obj['python'] = pyenv('bin','python')

            os.environ['PATH'] += ':'+pyenv('bin')

    ctx.obj['debug'] = ctx.obj['platform'] in ['local']
    ctx.obj.update(kwargs)

    #click.echo("Debug mode is %s" % ('on' if debug else 'off'))

    if 'GHOST_PERSON' in os.environ:
        ctx.obj['ghost'] = Ghost.from_env()
        
        ctx.obj['personna'] = ctx.obj['ghost'].specs
    else:
        ctx.obj['personna'] = None

    reactor.prepare(ctx)

    if os.path.exists(reactor.rpath()):
        os.chdir(reactor.rpath())
    else:
        print "Warning : Application's module '%s' not setup !" % os.environ['GHOST_MODULE']

########################################################################################

def process(soul, entry):
    entry['link'] = entry['link'] % soul
    entry['fqdn'] = '.'.join([entry['name'], soul.alias, soul.domain])

    return entry

@cli.command('list')
@click.option('-f','--field', default=None)
@click.pass_context
def listing(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    if ctx.obj['field'] is None:
        click.echo("Multiverse :")

    for ghost in Ghost.populate():
        if os.path.exists(upath('home', ghost, 'manifest.yml')):
            soul = Ghost.from_env(person=ghost)

            lst = [process(soul, entry) for entry in soul.specs.get('multiverse', [])]

            if ctx.obj['field'] in ('name','link','fqdn'):
                click.echo(' '.join([
                    entry[ctx.obj['field']]
                    for entry in lst
                    if ctx.obj['field'] in entry
                ]))
            else:
                click.echo("\t*) %(alias)s :" % soul)

                for entry in lst:
                    click.echo("")
                    click.echo("\t\t-> name\t: %(name)s" % entry)
                    click.echo("\t\t   link\t: %(link)s" % entry)
                    click.echo("\t\t   fqdn\t: %(fqdn)s" % entry)

#***************************************************************************************

@cli.command('shell')
@click.option('-i', '--interpreter', default='zsh')
@click.pass_context
def shell_cmd(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    if not os.path.exists('/usr/bin/%(interpreter)s' % ctx.obj):
        ctx.obj['interpreter'] = 'bash'

    shell(ctx.obj['interpreter'])

#***************************************************************************************

@cli.command()
@click.pass_context
def status(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo('Reactor :')
    click.echo('\t-> provider\t: %s' % reactor.provider)
    click.echo('\t-> narrow \t: %s' % reactor.narrow)

########################################################################################

@cli.command()
#@click.option('--count', default=1, help='Number of greetings.')
#@click.option('--skin', prompt=True, default=lambda: os.environ.get('GHOST_SKIN', 'color-admin,control-frog'))
@click.pass_context
def setup(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Python virtual env : %s" % pyenv())

    if not os.path.exists(pyenv()):
        shell('virtualenv', '--distribute', pyenv())

        shell(pyenv('bin', 'easy_install'), '-U', 'pip')

        shell(pyenv('bin', 'pip'), 'install', '-r', bpath('requirements.txt'))

    if os.path.exists(pyenv()):
        ctx.obj['python'] = pyenv('bin','python')
        ctx.obj['pip'] = pyenv('bin','pip')

    reactor.setup(ctx)

########################################################################################

@cli.command()
@click.option('--port', default=8000)
@click.pass_context
def web(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    #click.echo('Serving on http://0.0.0.0:%d/' % port)

    reactor.web(ctx)

#***************************************************************************************

@cli.command()
@click.option('--queue', default='default')
@click.pass_context
def worker(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Running worker on queue '%(queue)s' :" % ctx.obj)

    reactor.worker(ctx)

#***************************************************************************************

@cli.command()
@click.option('--flow', default='default')
@click.pass_context
def scheduler(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Running scheduler for flow '%(flow)s' :" % ctx.obj)

    reactor.scheduler(ctx)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
