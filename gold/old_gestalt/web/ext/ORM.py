#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

###################################################################################

@extend_reactor('orm')
class Extension(ReactorExt):
    def initialize(self):
        self._model = {
            'full':  {},
            'class': {},
            'alias': {},
        }

    models      = property(lambda self: self._model['alias'])

    #***************************************************************************

    def _status_sql(self, alias, link):
        try:
            return {
                "state": True,
                "table": sql_conns[alias].introspection.table_names(),
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

    ############################################################################

    def register_model(self, *args, **kwargs):
        def do_apply(handler, alias):
            app,key = handler._meta.app_label, handler._meta.db_table,

            if app not in self._model['full']:
                self._model['full'][app] = {}

            if key not in self._model['full'][app]:
                self._model['full'][app][key] = handler

            if alias not in self._model['alias']:
                self._model['alias'][alias] = handler

            if handler not in self._model['class']:
                self._model['class'][handler] = {}

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

