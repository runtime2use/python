#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

################################################################################

TESTERs = (
    'store',
    'cache',

    'sql',
    'rdf',

    'mongo',
    'neo4j',
)

@extend_reactor('admin')
class Extension(ReactorExt):
    def initialize(self):
        self._tests = [
            key.replace('_status_','')
            for key in dir(self)
            if key.startswith('_status_')
        ]

    @property
    def credentials(self):
        if not hasattr(self, '_creds'):
            from uchikoma import settings as stt

            setattr(self, '_creds', dict(
                platform={
                    'queue': (['failed']+[x for x in stt.RQ_QUEUES.keys()], {}),
                },
                backend={
                    'cache': stt.CACHES.iteritems(),

                    'sql':   stt.DATABASES.iteritems(),
                    'rdf':   stt.RDF_DATABASES.iteritems(),

                    'mongo': stt.MONGODB_DATABASES.iteritems(),
                    'neo4j': stt.NEO4J_DATABASES.iteritems(),
                },
                cloud=[
                    #('amazon',    '', (lambda x: x)),
                    ('google',    'https://status.cloud.google.com/incidents.json', (lambda x: x)),

                    #('facebook',  '', (lambda x: x)),
                    #('twitter',   'https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json', (lambda x: x)),

                    ('bitbucket', 'https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json', (lambda x: x)),
                    ('github',    'https://status.github.com/api/status.json', (lambda x: x)),

                    #('heroku',    'https://status.heroku.com/api/ui/availabilities?filter%5Bregion%5D=US&page%5Bsize%5D=60', (lambda x: x)),
                    ('heroku',    'https://status.heroku.com/api/ui/systems?include=open-incidents.tags%2Copen-incidents.current-update.service-statuses', (lambda x: x)),
                ],
            ))

        return getattr(self, '_creds', {})

    #***************************************************************************

    def get_platform_status(self, *aspects):
        resp,svcs = {}, self.credentials['platform']

        if len(aspects)==0:
            aspects = svcs.keys()

        for key in aspects:
                args,kwargs = svcs.get(key, ([],{}))

                etat = self._cached_status('platform', key, *args, **kwargs)

                if etat is None:
                    resp[key] = etat

        return resp

    #***************************************************************************

    def get_backends_status(self, *aspects):
        resp,creds = {}, self.credentials['backend']

        if len(aspects)==0:
            aspects = creds.keys()

        for key in aspects:
                hnd = getattr(self, '_status_%s' % key, None)

                if callable(hnd):
                    resp[key] = {}

                    for alias,link in creds.get(key,[]):
                        etat = self._cached_status('backend', key, alias,link)

                        if etat is None:
                            resp[key][alias] = etat
        
        return resp

    #***************************************************************************

    def get_cloud_status(self, *networks):
        resp,mesh = {}, self.credentials['cloud']

        if len(networks)==0:
            networks = [k for k,u,c in mesh]

        for key,url,cb in mesh:
                etat = self._cached_status('cloud', key, url, cb)

                if etat is None:
                    resp[key] = etat

        return resp

    #***************************************************************************

    @property
    def vault(self):
        from django.core.cache import caches

        return caches['default']

    cache_mapping = {
        'platform': (60 * 2, lambda provider, *args, **kwargs: '_'.join(['status', 'platform', provider])),
        'backend':  (60 * 2, lambda namespace, alias, link:    '_'.join(['status', 'backend',  namespace, alias])),
        'cloud':    (60 * 2, lambda provider, url, callback:   '_'.join(['status', 'cloud',    provider])),
    }

    def _cached_status(self, kind, *args, **kwargs):
        if kind in self.cache_mapping:
            timeout,localizer = self.cache_mapping[kind]

            narrow = localizer(*args, **kwargs)

            value = self.vault.get(narrow)

            if value is None:
                handler = getattr(self, '_cached_%s' % kind, None)

                if callable(handler):
                    value = handler(*args, **kwargs)

                self.vault.set(narrow, value, timeout)

            return value
        else:
            return None

    #***************************************************************************

    def _cached_platform(self, provider, *args, **kwargs):
        hnd = getattr(self, '_status_%s' % provider, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def _cached_backend(self, namespace, alias, link):
        hnd = getattr(self, '_status_%s' % namespace, None)

        if callable(hnd):
            resp = {}

            for alias,link in self.credentials.get(namespace,[]):
                resp[alias] = hnd(alias,link)

            return resp
        else:
            return None

    #***************************************************************************

    def _cached_cloud(self, provider, url, callback):
        req = requests.get(url)

        try:
            return callback(req.json())
        except Exception,ex:
            return {
                'error':   1,
                'message': str(ex),
            }

    #***************************************************************************

    def _status_rdf(self, alias, link):
        link['path'] = link['path'] or 'ds/query'

        if type(link.get('port',None)) in (int,str):
            link['netloc'] = '%(host)s:%(port)s' % link
        else:
            link['netloc'] = '%(host)s' % link

        link['netloc'] = 'http://%(netloc)s/%(path)s' % link

        proxy = SparqlWrapper(link['netloc'])

        proxy.setQuery("""
PREFIX foaf:  <http://xmlns.com/foaf/0.1/>

SELECT ?name WHERE {
    ?person foaf:name ?name .
}
              """)
        proxy.setReturnFormat(SparqlJSON)

        try:
            results = proxy.query().convert()

            return {
                "state": True,
            }
        except Exception,ex:
            return {
                "state": False,
                "error": str(ex),
            }

        return {"state": True}

