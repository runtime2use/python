#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

#*******************************************************************************

from django.conf.urls import url     as dj_url
from django.conf.urls import include as dj_include

from django.views.generic.base import View as DjangoView

################################################################################

class SubDomain(object):
    def __init__(self, prnt, app, name, icon=None, title=None):
        self._prnt  = prnt
        self._app   = app
        self._name  = name
        self._icon  = icon
        self._title = title

        self._rtr   = {}

    parent   = property(lambda self: self._prnt)

    app_name = property(lambda self: self._app)
    alias    = property(lambda self: self._name)

    icon     = property(lambda self: self._icon or '')
    title    = property(lambda self: self._title or self.alias.capitalize())

    template = property(lambda self: '%s/menu.html' % self.alias)
    ns_urls  = property(lambda self: 'uchikoma.%s.urls.%s' % (self.app_name, self.alias))
    ns_views = property(lambda self: 'uchikoma.%s.views.%s' % (self.app_name, self.alias))

    patterns = property(lambda self: self._rtr.values())

    @property
    def context(self):
        cnt = {}

        for src,trg in dict(
            alias='name',
            icon=None, title=None, template=None,
        ns_urls='urlconf').iteritems():
            cnt[trg or src] = getattr(self, src, None)

        return cnt

    #***************************************************************************

    @property
    def urlpatterns(self):
        resp = []

        for ptn in self.patterns:
            for url in ptn.django_urls():
                resp.append(url)

        return resp

################################################################################

class SubPattern(Helper):
    def initialize(self, pattern, alias, handler, strategy='anon', **kwargs):
        self._ptn   = pattern
        self._key   = alias

        self._hnd   = handler
        self._plc   = strategy
        self._opts  = kwargs

        self.prepare(**kwargs)

        if self.pattern not in self.domain._rtr:
            self.domain._rtr[self.pattern] = self

    domain   = property(lambda self: self.ancestor)
    policy   = property(lambda self: self._plc)

    pattern  = property(lambda self: self._ptn)
    alias    = property(lambda self: self._key or self.handler.__name__.lower())

    handler  = property(lambda self: self._hnd)
    options  = property(lambda self: self._opts)

    def __call__(self, request, *args, **kwargs):
        callback = self.invoke

        if self.policy=='login':
            from django.contrib.auth.decorators import login_required

            callback = login_required(callback)

        return callback(request, *args, **kwargs)

#*******************************************************************************

class WebFunc(SubPattern):
    def prepare(self):
        pass

    def invoke(self, request, *args, **kwargs):
        return self.handler(request, *args, **kwargs)

    def django_urls(self):
        yield dj_url(self.pattern, self, name=self.alias, **self.options)

#*******************************************************************************

class WebView(SubPattern):
    def prepare(self):
        self._rvw = self.handler()

    view  = property(lambda self: self._rvw)

    def invoke(self, request, *args, **kwargs):
        return self.view(request, *args, **kwargs)

    def django_urls(self):
        yield dj_url(self.pattern, self.handler.as_view(), name=self.alias, **self.options)

#*******************************************************************************

class WebREST(SubPattern):
    def prepare(self):
        self._rvw = self.handler()

    view  = property(lambda self: self._rvw)

    def invoke(self, request, *args, **kwargs):
        return self.view(request, *args, **kwargs)

    def django_urls(self):
        yield dj_url(self.pattern, dj_include(self.view.urls), name=self.alias, **self.options)

################################################################################

@extend_reactor('router')
class Extension(ReactorExt):
    def initialize(self):
        self._fqdn  = {}
        self._apis = {}

        lst = None

        if False:
            for app,ns in self.reactor.django.apps:
                lst = {}

                try:
                    mod = __import__('uchikoma.%s' % app)

                    if hasattr(mod, 'subdomains'):
                        lst = mod.subdomains
                except ImportError,ex:
                    print "FQDN '%s' : %s" % (app, ex)

                for name,item in lst.iteritems():
                    self.add_domain(app, name, **item)
        else:
            self.add_domain('connector',   'apis',     icon='key',            title="APIs")
            #self.add_domain('connector',   'connect',  icon='plug',           title="Connector")

            self.add_domain('organizer',   'contacts', icon='credit-card',    title="Contacts")
            self.add_domain('organizer',   'mail',     icon='envelope',       title="MailBox")
            #self.add_domain('organizer',   'chat',     icon='calendar',       title="Chat")
            self.add_domain('organizer',   'agenda',   icon='calendar',       title="Agenda")

            self.add_domain('console',     'console',    icon='tachometer',     title="Console")
            #self.add_domain('console',     'collab',   icon='random',         title="Collab")
            #self.add_domain('console',     'hub',      icon='server',         title="Hub")

            self.add_domain('entertain',   'cdn',      icon='rss',            title="CDN")
            self.add_domain('entertain',   'files',    icon='folder',         title="Files")
            self.add_domain('entertain',   'watch',    icon='video-camera',   title="Watch")
            #self.add_domain('entertain',   'see',      icon='camera',         title="See")
            self.add_domain('entertain',   'listen',   icon='music',          title="Listen")
            #self.add_domain('entertain',   'read',     icon='file-text',      title="Readings")

            self.add_domain('cables',      'cables',     icon='rss',            title="Cables")
            self.add_domain('cables',      'news',     icon='rss',            title="Newswire")
            self.add_domain('cables',      'social',   icon='users',          title="Social Networking")

            self.add_domain('devops',      'cloud',    icon='cloud',          title="Cloud")
            self.add_domain('devops',      'dev',      icon='code',           title="Dev")
            self.add_domain('devops',      'ops',      icon='gears',          title="Ops")
            self.add_domain('devops',      'fleet',    icon='plane',          title="Fleet")

            self.add_domain('linguistics', 'linguist', icon='flag',           title="Linguist")

            self.add_domain('semantics',   'meta',     icon='diamond',        title="Meta")
            self.add_domain('semantics',   'schema',   icon='sitemap',        title="Schema")
            self.add_domain('semantics',   'graph',    icon='connectdevelop', title="Graph")

            self.add_domain('opendata',    'geo',      icon='globe',          title="Geo")
            self.add_domain('opendata',    'data',     icon='database',       title="Data")
            self.add_domain('opendata',    'linked',   icon='link',           title="Linked")

            self.add_domain('landing',     'www',      icon='server',         title="Frontend")
            #self.add_domain('landing',     'blog',     icon='server',         title="Frontend")

    #***************************************************************************

    subdomains = property(lambda self: self._fqdn)

    def add_domain(self, *args, **kwargs):
        fqdn = SubDomain(self, *args, **kwargs)

        if fqdn.alias in self._fqdn:
            fqdn = self._fqdn[fqdn.alias]
        else:
            self._fqdn[fqdn.alias] = fqdn

        return fqdn

    #***************************************************************************

    def urlpatterns(self, subdomain, *routes):
        resp = [r for r in routes]

        if subdomain in self._fqdn:
            fqdn = self._fqdn[subdomain]

            resp += fqdn.urlpatterns

        return resp

    #***************************************************************************

    def apipatterns(self, *routes):
        from tastypie.api import Api

        resp,mgr = [], {}

        for subdomain in self._route:
            mgr[subdomain] = Api()

            for entry in self._route[subdomain]:
                if 'rest' in entry:
                    hnd = entry['rest']

                    obj = hnd()

                    mgr[subdomain].register(obj)

            resp.append(dj_url(re.compile('^%s/' % subdomain), dj_include(mgr[subdomain].urls)))

        return resp

    ############################################################################

    def register_route(self, *args, **kwargs):
        def do_apply(handler, subdomain, pattern, alias=None, **options):
            if subdomain in self._fqdn:
                wrapper = WebFunc

                if isclass(handler):
                    if issubclass(handler, DjangoView):
                        wrapper = WebView

                entry = wrapper(self._fqdn[subdomain], pattern, alias, handler, **options)

                return handler
            else:
                print "Unknown subdomain '%s' with pattern %s for '%s' !" % (subdomain,pattern, handler)

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    def register_func(self, *args, **kwargs):
        def do_apply(handler, subdomain, pattern, alias=None):
            entry = {
                'rule': pattern,
                'name': alias or '.'.join([handler.__module__,handler.__name__]),
                'fqdn': subdomain,
                'func': handler,
            }

            if subdomain not in self._route:
                self._route[subdomain] = []

            self._route[subdomain].append(entry)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_view(self, *args, **kwargs):
        def do_apply(handler, pattern, alias=None, subdomain=None):
            entry = {
                'rule': pattern,
                'name': alias or '.'.join([handler.__module__,handler.__name__]),
                'fqdn': subdomain,
                'view': handler,
            }

            if subdomain not in self._route:
                self._route[subdomain] = []

            self._route[subdomain].append(entry)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_rest(self, *args, **kwargs):
        def do_apply(handler, pattern, alias=None, subdomain=None):
            entry = {
                'rule': pattern,
                'name': alias or '.'.join([handler.__module__,handler.__name__]),
                'fqdn': subdomain,
                'rest': handler,
            }

            if subdomain not in self._route:
                self._route[subdomain] = []

            self._route[subdomain].append(entry)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

