from django.conf.urls import patterns, include, url

#*******************************************************************************

from django.contrib import admin
admin.autodiscover()

from neo4django import admin as neo_admin
neo_admin.autodiscover()

################################################################################

urlpatterns = [
    url(r'^manage/django/',   include(admin.site.urls)),
    url(r'^manage/queues/',   include('django_rq.urls')),

    url(r'^status/reports/',  include('report_builder.urls')),

    #url(r'^',          include('admin_honeypot.urls', namespace='admin_honeypot')),
]

