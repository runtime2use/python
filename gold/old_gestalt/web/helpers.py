from gestalt.web.libs import *

#*******************************************************************************

from gestalt.core.utils import *

###################################################################################

@singleton(None)
class Reactor(Nucleon):
    def loading(self):
        import gestalt.web.ext

        #for app in self.applications:

        for app in ['cables','connector','entertain','opendata','organizer','semantics','linguistics']:
            try:
                __import__('%s.%s.ext.Reactor' % (self.namespace, app))
            except NameError,ex:
                pass
            except Exception,ex:
                print "Exception while loading Reactor extensions for '%s' :" % app

                raise ex

    def loaded(self):
        return

        for app in self.applications:
            try:
                __import__('%s.%s.ext.APIs' % (self.namespace, app))
            except NameError,ex:
                pass
            except Exception,ex:
                print "Exception while loading Reactor extensions for '%s' :" % app

                raise ex

###################################################################################

class DjangoContext(Nucleon):
    request  = property(lambda self: self.ancestor)

    identity = property(lambda self: self.request.user)

