#!/bin/bash

startswith () {
    result=`python -c "print '$1'.startswith('$2')"`

    if [[ $result == "True" ]] ; then
        return 0
    else
        return 1
    fi
}

endswith () {
    result=`python -c "print '$1'.endswith('$2')"`

    if [[ $result == "True" ]] ; then
        return 0
    else
        return 1
    fi
}

instr () {
    result=`python -c "print '$1' in '$2'"`

    if [[ $result == "True" ]] ; then
        return 0
    else
        return 1
    fi
}

#**************************************************************************

replace () {
    python -c "print '$1'.replace('$2','$3')"
}

