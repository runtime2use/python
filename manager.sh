#!/bin/bash

export PYPI_REPO_PORT=65012
export PYPI_REPO_PATH="$RONIN_MONT/proxies/pypi"
#export PYPI_REPO_TIME=60000

################################################################################

noh_language_python_cacher () {
    if [[ ! -d $PYPI_REPO_PATH ]] ; then
        mkdir -p $PYPI_REPO_PATH

        devpi-server --port $PYPI_REPO_PORT --serverdir $PYPI_REPO_PATH --init
    fi

    devpi-server --port $PYPI_REPO_PORT --serverdir $PYPI_REPO_PATH
}

#*******************************************************************************


